<?php

$cognito_auth_namespace = '\TangleMedia\Laravel\CognitoAuth\Http\Controllers\\';

Route::group([
    'prefix' => 'users'
], function () use ($cognito_auth_namespace) {
    $controller = $cognito_auth_namespace . 'UserController';
	Route::get('/', $controller.'@index')->name('api.v1.users.get');
	Route::get('/{user_id}', $controller.'@show')->name('api.v1.users.show');
	Route::post('/', $controller.'@store')->name('api.v1.users.store');
	Route::put('/{user_id}', $controller.'@update')->name('api.v1.users.update');
	Route::delete('/{user_id}', $controller.'@delete')->name('api.v1.users.delete');
});

Route::group([
    'prefix' => 'auth'
],function () use ($cognito_auth_namespace) {
    $controller = $cognito_auth_namespace . 'AuthController';
    Route::group(['prefix' => 'sign-up'], function () use ($controller) {
        Route::post('/', $controller.'@signUp')->name('api.v1.auth.sign_up');
        Route::post('/guest', $controller.'@signUpGuest')->name('api.v1.auth.sign_up_guest');
    });
    Route::post('/resend-confirmation', $controller.'@resendConfirmation')->name('api.v1.auth.resend_confirmation');
    Route::post('/confirm-sign-up', $controller.'@confirmSignUp')->name('api.v1.auth.confirm_sign_up');
    Route::post('/invite', $controller.'@invite')->name('api.v1.auth.invite');
    Route::post('/login', $controller.'@login')->name('api.v1.auth.login');
    Route::post('/logout', $controller.'@logout')->name('api.v1.auth.logout');
    Route::post('/change-password', $controller.'@changePassword')->name('api.v1.auth.change_password');
    Route::post('/refresh', $controller.'@refresh')->name('api.v1.auth.refresh');
});
