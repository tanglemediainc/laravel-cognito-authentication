<?php

return [

    'models' => [
        'user' => TangleMedia\Laravel\CognitoAuth\Models\User::class,
    ],

    'resources' => [
        'user' => TangleMedia\Laravel\CognitoAuth\Http\Resources\UserResource::class,
    ],

    'services' => [
        'user' => TangleMedia\Laravel\CognitoAuth\Services\UserService::class,
    ],

    'repositories' => [
        'user' => TangleMedia\Laravel\CognitoAuth\Repositories\UserRepository::class,
    ],

    'table_names' => [
        'users' => 'users',
    ],

    'uses_uuid' => false,

    'aws_access_key_id' => env('AWS_ACCESS_KEY_ID', null),
    'aws_secret_access_key' => env('AWS_SECRET_ACCESS_KEY', null),
    'cognito_auth_region' => env('COGNITO_AUTH_REGION', null),
    'cognito_user_pool_id' => env('COGNITO_USER_POOL_ID', null),
    'cognito_client_id' => env('COGNITO_CLIENT_ID', null),

];
