# Tangle Media Cognito Auth Service

## Installation

1. This package publishes a `config/users.php` file. If you already have a file by that name, you must rename or remove it.

2. You can install the package via composer:
   `composer require tangle-media/laravel-users`

3. Optional: The service provider will automatically get registered. Or you may manually add the service provider in your `config/app.php` file:

   ```
   'providers' => [
       // ...
       TangleMedia\Laravel\CognitoAuth\UserServiceProvider::class,
   ];
   ```

4. You should publish the migration and the `config/users.php` config file with:

   ```
   php artisan vendor:publish --provider="TangleMedia\Laravel\CognitoAuth\UserServiceProvider"
   ```

5. NOTE: If you are using UUIDs, ~~see the Advanced section of the docs on UUID steps, before you continue. It explains some changes you may want to make to the migrations and config file before continuing. It also mentions important considerations after extending this package’s models for UUID capability.~~ make sure to updated the config file.

6. Run the migrations: After the config and migration have been published and configured, you can create the tables for this package by running:

   ```
   php artisan migrate
   ```

   ​