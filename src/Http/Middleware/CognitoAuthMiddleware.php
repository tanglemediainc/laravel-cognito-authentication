<?php

namespace TangleMedia\Laravel\CognitoAuth\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use TangleMedia\Laravel\CognitoAuth\Helpers\CognitoJWT;
use TangleMedia\Laravel\CognitoAuth\Interfaces\Repository\UserServiceInterface;

class CognitoAuthMiddleware
{

    protected $service;

    /**
     * UserController constructor.
     * @param UserServiceInterface $service
     */
    public function __construct(UserServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if($request->headers->has('x-forwarded-method') &&
            strtoupper($request->headers->get('x-forwarded-method')) === 'OPTIONS') {
            return $next($request);
        }

        $jwt = $request->bearerToken();
        if (!$jwt) {
            abort(403, 'No jwt token');
        }

        $region = config('cognito_auth.cognito_auth_region', null);
        $user_pool_id = config('cognito_auth.cognito_user_pool_id', null);
        if (!$region || !$user_pool_id) {
            abort(403, 'Missing cognito configuration');
        }

        $decoded = CognitoJWT::verifyToken($jwt, $region, $user_pool_id);
        if (!$decoded || !($decoded instanceof \stdClass)) {
            abort(403, 'Could not decode verified token');
        }

        $user = $this->service->getOneFromCognito($decoded->username);
        Auth::setUser($user);

        return $next($request);

    }

}
