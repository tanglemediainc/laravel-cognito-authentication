<?php

namespace TangleMedia\Laravel\CognitoAuth\Http\Traits;

use Illuminate\Http\JsonResponse;

trait AuthResponse
{

    /**
     * @param $message
     * @param int $code
     * @return JsonResponse
     */
    public function errorResponse($message, $code = 500) : JsonResponse
    {
        return (new JsonResponse((is_array($message) ? $message : ['error' => (string)$message]), $code));
    }

    /**
     * @param array $data
     * @param int $code
     * @return JsonResponse
     */
    public function successResponse(array $data, $code = 200) : JsonResponse
    {
        return (new JsonResponse($data, $code));
    }

}
