<?php

namespace TangleMedia\Laravel\CognitoAuth\Http\Controllers;

use App\Exceptions\UnauthorizedPermissionsException;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use TangleMedia\Laravel\CognitoAuth\Helpers\UserUploadFile;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\UserStoreRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\UserUpdateRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Resources\UserResource;
use TangleMedia\Laravel\CognitoAuth\Http\Traits\JResponse;
use TangleMedia\Laravel\CognitoAuth\Interfaces\Repository\UserServiceInterface;

class UserController extends Controller
{

    use JResponse;

    protected $service;

    /**
     * UserController constructor.
     * @param UserServiceInterface $service
     */
    public function __construct(UserServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Get paginated
     * @return JsonResponse
     * @throws UnauthorizedPermissionsException
     */
    public function index()
    {
        $records = $this->service->getPaginated();
        return $this->successResponse('user_list', [
            'user' => UserResource::collection($records),
            'pagination' => new \App\Http\Resources\PaginationResource($records)
        ]);
    }

    /**
     * Get single
     * @param string $id
     * @return JsonResponse
     * @throws UnauthorizedPermissionsException
     */
    public function show(string $id)
    {
        $record = $this->service->getOne($id);
        return $this->successResponse('show_user', [
            'user' => new UserResource($record),
            'pagination' => null
        ]);
    }

    /**
     * Create new lot type horse
     * @param UserStoreRequest $request
     * @return JsonResponse
     * @throws UnauthorizedPermissionsException
     */
    public function store(UserStoreRequest $request)
    {
        $record = $this->service->store($request->all());
        return $this->successResponse('user_created', [
            'user' => new UserResource($record),
            'pagination' => null
        ]);
    }

    /**
     * Update lot type horse
     * @param UserUpdateRequest $request
     * @param string $id
     * @return JsonResponse
     * @throws UnauthorizedPermissionsException
     */
    public function update(UserUpdateRequest $request, string $id)
    {
        $record = $this->service->update($request->all(), $id);
        return $this->successResponse('user_updated', [
            'user' => new UserResource($record),
            'pagination' => null
        ]);
    }

    /**
     * Delete lot type horse
     * @param string $id
     * @return JsonResponse
     * @throws UnauthorizedPermissionsException
     */
    public function delete(string $id)
    {
        $this->service->delete($id);
        return $this->successResponse('user_deleted');
    }
}
