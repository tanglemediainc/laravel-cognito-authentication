<?php

namespace TangleMedia\Laravel\CognitoAuth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use TangleMedia\Laravel\CognitoAuth\Helpers\CognitoWrapper;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\ChangePasswordRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\ConfirmPasswordResetRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\ConfirmSignUpRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\InviteRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\LoginRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\RefreshRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\ResendConfirmationRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\SendPasswordResetRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\SignUpGuestRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\SignUpRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\UserUpdateAttributesRequest;
use TangleMedia\Laravel\CognitoAuth\Http\Resources\UserResource;
use TangleMedia\Laravel\CognitoAuth\Http\Traits\AuthResponse;
use TangleMedia\Laravel\CognitoAuth\Interfaces\Repository\UserServiceInterface;
use TangleMedia\Laravel\CognitoAuth\Translators\UserResourceTranslator;

class AuthController extends Controller
{

    use AuthResponse;

    protected $wrapper;
    protected $user_service;
    protected $user_resource_translator;

    public function __construct(UserServiceInterface $user_service, Request $request)
    {
        $this->user_service = $user_service;
        $this->wrapper = new CognitoWrapper($request->bearerToken());
        $this->wrapper->initialize();
        $this->user_resource_translator = new UserResourceTranslator();
    }

    public function signUp(SignUpRequest $request)
    {
        $data = $this->wrapper->signUp($request->input('username'), $request->input('username'), $request->input('password'));
        if(isset($data['cognito_user'])) {
            if(isset($data['cognito_user']['confirmed']) && !$data['cognito_user']['confirmed']) {
                $data['cognito_user']['username'] = $request->input('username');
                $data['cognito_user']['email'] = $request->input('username');
            }
            $user = $this->user_service->store(['cognito_id' => $data['cognito_user']['sub'], 'email' => $data['cognito_user']['email']]);
            $data['user'] = $this->user_resource_translator->translate($user);
        }
        return $this->successResponse($data, 201);
    }

    public function signUpGuest(SignUpGuestRequest $request)
    {
        $this->middleware('cognito-auth');
        $username = $request->has('username') ? $request->input('username') : Str::uuid();
        $data = $this->wrapper->signUp($username, $request->input('email'), $request->input('password'));
        if(isset($data['cognito_user'])) {
            if(isset($data['cognito_user']['confirmed']) && !$data['cognito_user']['confirmed']) {
                $data['cognito_user']['username'] = $username;
                $data['cognito_user']['email'] = $request->input('email');
                $confirm = $this->wrapper->adminConfirmSignUp($request->input('username'));
                if($confirm) {
                    $data['cognito_user']['confirmed'] = true;
                }
            }
            $user = $this->user_service->store(['username' => $username, 'email' => $data['cognito_user']['email']]);
            $data['user'] = $this->user_resource_translator->translate($user);
        }
        return $this->successResponse($data, 201);
    }

    public function resendConfirmation(ResendConfirmationRequest $request)
    {
        $this->middleware('cognito-auth');
        $data = $this->wrapper->resendConfirmationCode($request->input('username'));
        return $this->successResponse($data);
    }

    public function confirmSignUp(ConfirmSignUpRequest $request)
    {
        $this->middleware('cognito-auth');
        $data = $this->wrapper->confirmSignUp($request->input('username'), $request->input('code'));
        return $this->successResponse($data);
    }

    public function invite(InviteRequest $request)
    {
        $this->middleware('cognito-auth');
        $password = $request->has('password') ? $request->input('password') : Str::random(16).'!';
        $data = $this->wrapper->adminCreateUser($request->input('email'), $request->input('email'), $password);
        if(isset($data['cognito_user'])) {
            $user = $this->user_service->store(['cognito_id' => $data['cognito_user']['sub'], 'email' => $data['cognito_user']['email']]);
            $data['user'] = $this->user_resource_translator->translate($user);
        }
        return $this->successResponse($data, 201);
    }

    public function login(LoginRequest $request)
    {
        $data = $this->wrapper->authenticate($request->input('username'), $request->input('password'));
        if(isset($data['cognito_user']['username'])) {
            $user = $this->user_service->getOneFromCognito($data['cognito_user']['username']);
            $data['user'] = $this->user_resource_translator->translate($user);
        }
        return $this->successResponse($data);
    }

    public function challengePasswordReset(LoginRequest $request)
    {
        $data = $this->wrapper->resetChallenge($request->input('username'), $request->input('password'), $request->input('session'));
        return $this->successResponse($data);
    }

//    public function changePassword(ChangePasswordRequest $request)
//    {
//        $this->middleware('cognito-auth');
//        $data = $this->wrapper->resetPassword($request->input('code'), $request->input('password'), $request->input('username'));
//        return $this->successResponse($data);
//    }

    public function sendPasswordReset(SendPasswordResetRequest $request)
    {
        $data = $this->wrapper->sendPasswordResetMail($request->input('username'));
        return $this->successResponse($data);
    }

    public function confirmPasswordReset(ConfirmPasswordResetRequest $request)
    {
        $data = $this->wrapper->resetPassword($request->input('code'), $request->input('password'), $request->input('username'));
        return $this->successResponse($data);
    }

    public function refresh(RefreshRequest $request)
    {
        $this->middleware('cognito-auth');
        $data = $this->wrapper->refreshToken($request->input('token'));
        if(isset($data['cognito_user'])) {
            if(isset($data['cognito_user']['confirmed']) && !$data['cognito_user']['confirmed']) {
                $data['cognito_user']['username'] = $request->input('username');
                $data['cognito_user']['email'] = $request->input('username');
            }
            $user = $this->user_service->getOneFromCognito($data['cognito_user']['sub']);
            $data['user'] = $this->user_resource_translator->translate($user);
        }
        return $this->successResponse($data);
    }

    public function updateUser(UserUpdateAttributesRequest $request)
    {
        $data = $this->wrapper->updateUser($request->input('username'), $request->input('attributes'));
        return $this->successResponse($data);
    }

}
