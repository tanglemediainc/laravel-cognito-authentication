<?php

namespace TangleMedia\Laravel\CognitoAuth;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use TangleMedia\Laravel\CognitoAuth\Http\Middleware\CognitoAuthMiddleware;
use TangleMedia\Laravel\CognitoAuth\Interfaces\Models\UserInterface;
use TangleMedia\Laravel\CognitoAuth\Interfaces\Repositories\UserRepositoryInterface;
use TangleMedia\Laravel\CognitoAuth\Interfaces\Repository\UserServiceInterface;

class CognitoAuthServiceProvider extends ServiceProvider
{

    public function boot(Filesystem $filesystem)
    {
        if (function_exists('config_path')) {
            $this->publishes([
                __DIR__ . '/../config/cognito_auth.php' => config_path('cognito_auth.php'),
            ], 'config');
            $this->publishes([
                __DIR__ . '/../database/migrations/create_cognito_auth_tables.php.stub' => $this->getMigrationFileName($filesystem),
            ], 'migrations');
        }

        $this->commands([]);

        $this->registerBindings();

        $this->app->router->aliasMiddleware('auth.cognito', CognitoAuthMiddleware::class);
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/cognito_auth.php',
            'cognito_auth'
        );
    }

    protected function registerBindings()
    {
        $models = $this->app->config['cognito_auth.models'];
        $services = $this->app->config['cognito_auth.services'];
        $repositories = $this->app->config['cognito_auth.repositories'];
        if(!$models || !$services || !$repositories) {
            return;
        }

//        dd(
//            UserInterface::class,
//            UserServiceInterface::class,
//            UserRepositoryInterface::class,
//            $models['user'],
//            $services['user'],
//            $repositories['user']
//        );

        $this->app->bind(UserInterface::class, $models['user']);
        $this->app->bind(UserServiceInterface::class, $services['user']);
        $this->app->bind(UserRepositoryInterface::class, $repositories['user']);
    }

    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @param Filesystem $filesystem
     * @return string
     */
    protected function getMigrationFileName(Filesystem $filesystem): string
    {
        $timestamp = date('Y_m_d_His');

        return Collection::make($this->app->databasePath().DIRECTORY_SEPARATOR.'migrations'.DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem) {
                return $filesystem->glob($path.'*create_cognito_auth_tables.php');
            })->push($this->app->databasePath()."/migrations/{$timestamp}create_cognito_auth_tables.php")
            ->first();
    }
}
