<?php

namespace TangleMedia\Laravel\CognitoAuth\Helpers;

use Aws\CognitoIdentityProvider\CognitoIdentityProviderClient;
use Aws\Exception\AwsException;
use Aws\Result;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class CognitoWrapper
{

    /**
     * @var string|null
     */
    private $access_token;

    /**
     * @var array|false|string
     */
    private $region;

    /**
     * @var array|false|string
     */
    private $client_id;

    /**
     * @var array|false|string
     */
    private $user_pool_id;

    /**
     * @var CognitoIdentityProviderClient|null
     */
    private $client;

    /**
     * @var Result|null
     */
    private $result = null;

    public function __construct(string $access_token = null)
    {
        $region = config('cognito_auth.cognito_auth_region', null);
        $user_client_id = config('cognito_auth.cognito_client_id', null);
        $user_pool_id = config('cognito_auth.cognito_user_pool_id', null);
        if(!$region || !$user_client_id || !$user_pool_id) {
            throw new \InvalidArgumentException("Please provide the cognito configuration.");
        }
        $this->region = $region;
        
        $this->client_id = $user_client_id;
        $this->user_pool_id = $user_pool_id;
        $this->access_token = $access_token;
    }

    /* ===================================================== *
     * INITIALIZE THE WRAPPER
     * ===================================================== */

    public function initialize() : void
    {
        $this->client = new CognitoIdentityProviderClient([
            'version' => '2016-04-18',
            'region' => $this->region,
        ]);
        try {
            $this->user = $this->client->getUser([
                'AccessToken' => $this->access_token
            ]);
        } catch(\Exception  $e) {
            $this->user = null;
        }
    }

    /* ===================================================== *
     * REQUEST ACTION METHODS
     * ===================================================== */

    public function authenticate(string $username, string $password) : array
    {
        //dd(getenv('AWS_ACCESS_KEY_ID'));
        $result = $this->client->adminInitiateAuth([
            'AuthFlow' => 'ADMIN_NO_SRP_AUTH',
            'ClientId' => $this->client_id,
            'UserPoolId' => $this->user_pool_id,
            'AuthParameters' => [
                'USERNAME' => $username,
                'PASSWORD' => $password,
            ],
        ]);
        return $this->transformSignInResult($result);
//        if($result) {
//            $user = $this->client->adminGetUser([
//                'UserPoolId' => $this->user_pool_id,
//                'Username' => $username,
//            ]);
//            if($user) {
//                $user_id = $user->get('Username');
//            }
//        }
//        $data = [];
//        if($result['ChallengeName'] === "NEW_PASSWORD_REQUIRED")
//        {
//            $data['state'] = 'challenge';
//            $data['challenge'] = $result['ChallengeName'];
//            $data['session'] = $result['Session'];
//            return $data;
//        }
//        $data['state'] = 'okay';
//        $data['id'] = $result->get('AuthenticationResult')['IdToken'];
//        $data['uuid'] = $user_id;
//        $data['token'] = $result->get('AuthenticationResult')['AccessToken'];
//        $data['refresh'] = $result->get('AuthenticationResult')['RefreshToken'];
//        return $data;
    }

    public function refreshToken(string $refreshToken) : array
    {
        $result = $this->client->adminInitiateAuth([
            'AuthFlow' => 'REFRESH_TOKEN_AUTH',
            'AuthParameters' => [
                'REFRESH_TOKEN' => $refreshToken
            ],
            'ClientId' => $this->client_id,
            'UserPoolId' => $this->user_pool_id,
        ]);
        return $this->transformSignInResult($result);
//        $data = [];
//        $data['state'] = 'okay';
//        $data['id'] = $result->get('AuthenticationResult')['IdToken'];
//        $data['token'] = $result->get('AuthenticationResult')['AccessToken'];
//        if(isset($result->get('AuthenticationResult')['RefreshToken'])) {
//            $data['refresh'] = $result->get('AuthenticationResult')['RefreshToken'];
//        }
//        return $data;
    }

    public function resetChallenge(string $username, string $password, string $session) : array
    {
        $this->client->respondToAuthChallenge([
            'ChallengeName' => 'NEW_PASSWORD_REQUIRED',
            'ClientId' => $this->client_id,
            'ChallengeResponses' => [
                'USERNAME' => $username,
                'NEW_PASSWORD' => $password,
            ],
            'Session' => $session,
        ]);
        return [];
    }

    public function adminCreateUser(string $username, string $email, string $password) : array
    {
        $result = $this->client->adminCreateUser([
            'UserPoolId' => $this->user_pool_id,
            'Username' => $username,
            'TemporaryPassword' => $password,
            'UserAttributes' => [
                [
                    'Name' => 'name',
                    'Value' => $username
                ],
                [
                    'Name' => 'email',
                    'Value' => $email
                ],
                [
                    'Name' => 'email_verified',
                    'Value' => 'true'
                ]
            ],
        ]);
        return $this->transformSignUpResult($result);
//        $user = $result->get('User');
//        $data = [];
//        $data['state'] = 'okay';
//        $data['id'] = $user['Username'];
//        $data['username'] = $username;
//        $data['email'] = $email;
//        $data['confirmed'] = (bool)$result->get('UserConfirmed');
//        return $data;
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $password
     * @return JsonResponse
     */
    public function signUp(string $username, string $email, string $password) : array
    {
        $result = $this->client->signUp([
            'ClientId' => $this->client_id,
            'Username' => $username,
            'Password' => $password,
            'UserAttributes' => [
                [
                    'Name' => 'name',
                    'Value' => $username
                ],
                [
                    'Name' => 'email',
                    'Value' => $email
                ]
            ],
        ]);
        return $this->transformSignUpResult($result);
//        $data = [];
//        $data['state'] = 'okay';
//        $data['id'] = $result->get('UserSub');
//        $data['username'] = $username;
//        $data['email'] = $email;
//        $data['confirmed'] = (bool)$result->get('UserConfirmed');
//        return $data;
    }

    public function resendConfirmationCode(string $username) : array
    {
        $this->client->resendConfirmationCode([
            'ClientId' => $this->client_id,
            'Username' => $username
        ]);
        return [];
    }

    public function confirmSignUp(string $username, string $code) : array
    {
        $this->client->confirmSignUp([
            'ClientId' => $this->client_id,
            'Username' => $username,
            'ConfirmationCode' => $code,
        ]);
        return [];
    }

    public function adminConfirmSignUp(string $username) : array
    {
        $this->client->adminConfirmSignUp([
            'UserPoolId' => $this->user_pool_id,
            'Username' => $username,
        ]);
        return [];
    }

    public function sendPasswordResetMail(string $username) : array
    {
        $this->client->forgotPassword([
            'ClientId' => $this->client_id,
            'Username' => $username
        ]);
        return [];
    }

    public function resetPassword(string $code, string $password, string $username) : array
    {
        $this->client->confirmForgotPassword([
            'ClientId' => $this->client_id,
            'ConfirmationCode' => $code,
            'Password' => $password,
            'Username' => $username
        ]);
        return [];
    }

    public function addUserToGroup(string $username, string $group) : array
    {
        $this->client->adminAddUserToGroup([
            'UserPoolId' => $this->user_pool_id,
            'Username' => $username,
            'GroupName' => $group,
        ]);
        return [];
    }

    public function removeUserFromGroup(string $username, string $group) : array
    {
        $this->client->adminRemoveUserFromGroup([
            'UserPoolId' => $this->user_pool_id,
            'Username' => $username,
            'GroupName' => $group,
        ]);
        return [];
    }

    public function updateUser(string $username, array $attrs) : array
    {
        $valid_keys = [
            'address', 'birthdate', 'email', 'family_name', 'gender', 'given_name', 'locale', 'middle_name', 'name', 
            'nickname', 'phone_number', 'picture', 'preferred_username', 'profile', 'updated_at', 'website', 'zoneinfo',
            'email_verified'
        ];
        $valid_attrs = [];
        foreach($attrs as $k => $v) {
            if(in_array($k, $valid_keys)) {
                $valid_attrs[] = [
                    'Name' => $k,
                    'Value' => (string)$v
                ];
            }
        }
        $this->client->adminUpdateUserAttributes([
            'Username' => $username,
            'UserPoolId' => $this->user_pool_id,
            'UserAttributes' => $valid_attrs,
        ]);
        return [];
    }

    /* ===================================================== *
     * UTILITY AND HELPER METHODS
     * ===================================================== */

    public function transformUserResult($user) : array
    {
        $user_data = [];
        $user_data['username'] = $user['Username'];
        $attrs = (isset($user['Attributes']) ? $user['Attributes'] : (isset($user['UserAttributes']) ? $user['UserAttributes'] : []));
        foreach($attrs as $attribute) {
            $user_data[$attribute['Name']] = $attribute['Value'];
        }
        $user_data['created_at'] = Carbon::parse($user['UserCreateDate']);
        $user_data['updated_at'] = Carbon::parse($user['UserLastModifiedDate']);
        $user_data['enabled'] = $user['Enabled'];
        $user_data['status'] = $user['UserStatus'];
        return $user_data;
    }

    public function transformSignUpResult($result) : array
    {
        $user_data = [];
        if($result->get('User')) {
            $user_data = $this->transformUserResult($result->get('User'));
        }
        $user_data['sub'] = (!isset($user_data['sub']) ? $result->get('UserSub') : $user_data['sub']);
        $user_data['confirmed'] = (bool)$result->get('UserConfirmed');
        return ['cognito_user' => $user_data];
    }

    public function transformSignInResult($result) : array
    {
        $sign_in = [];
        $auth_result = $result->get('AuthenticationResult');
        if($auth_result) {
            $sign_in['access_token'] = $auth_result['AccessToken'];
            $sign_in['expires_at'] = Carbon::now()->add(new \DateInterval('PT'.$auth_result['ExpiresIn'].'S'));
            if(isset($auth_result['RefreshToken'])) {
                $sign_in['refresh_token'] = $auth_result['RefreshToken'];
            }
            $sign_in['id_token'] = $auth_result['IdToken'];
        }
        $sign_in['challenge'] = $result->get('ChallengeName');
        $sign_in['session'] = $result->get('Session');
        if($result->get('ChallengeParameters') && $result->get('ChallengeParameters')['userAttributes']) {
            foreach(json_decode($result->get('ChallengeParameters')['userAttributes'], true) as $k => $v) {
                $sign_in[$k] = $v;
            }
        }
        if(isset($sign_in['access_token'])) {
            $user = $this->client->getUser(['AccessToken' => $sign_in['access_token']]);
            if($user) {
                $sign_in['cognito_user'] = $this->transformUserResult($user);
            }
        }
        $sign_in['cognito_user'] = !isset($sign_in['cognito_user']) ? null : $sign_in['cognito_user'];
        return $sign_in;
    }

//
//    /**
//     * @return bool
//     */
//    public function isAuthenticated() : bool
//    {
//        return null !== $this->user;
//    }
//
//    /**
//     * @return array
//     */
//    public function getPoolMetadata() : array
//    {
//        $result = $this->client->describeUserPool([
//            'UserPoolId' => $this->user_pool_id,
//        ]);
//        return $result->get('UserPool');
//    }
//
//    /**
//     * @return array
//     */
//    public function getPoolUsers() : array
//    {
//        $result = $this->client->listUsers([
//            'UserPoolId' => $this->user_pool_id,
//        ]);
//        return $result->get('Users');
//    }
//
//    /**
//     * @return Result|null
//     */
//    public function getUser() : ?\Aws\Result
//    {
//        return $this->user;
//    }

}
