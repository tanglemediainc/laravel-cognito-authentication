<?php

namespace TangleMedia\Laravel\CognitoAuth\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use TangleMedia\Laravel\CognitoAuth\Interfaces\Models\UserInterface;
use TangleMedia\Laravel\CognitoAuth\Filters\UserFilter;

class User extends Authenticatable implements UserInterface
{

    protected $guarded = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTable(config('cognito_auth.table_names.users'));
    }

    /**
     * @param Builder $builder
     * @param $request
     * @return mixed
     */
    public function scopeFilter(Builder $builder, $request)
    {
        return (new UserFilter($request))->filter($builder);
    }

    public function scopeHash(Builder $builder, $hash)
    {
        return $builder->where('hash', '=', $hash);
    }

}
