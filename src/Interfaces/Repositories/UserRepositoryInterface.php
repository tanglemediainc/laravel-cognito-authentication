<?php

namespace TangleMedia\Laravel\CognitoAuth\Interfaces\Repositories;

interface UserRepositoryInterface
{
    public function paginate();
    public function findOne(string $id);
    public function findOneFromCognito(string $cognito_id);
    public function create(array $data);
    public function update(array $data, string $id);
    public function delete(string $id);
}
