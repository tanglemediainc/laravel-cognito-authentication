<?php

namespace TangleMedia\Laravel\CognitoAuth\Interfaces\Repository;

interface UserServiceInterface
{
    public function getPaginated();
    public function getOne(string $id);
    public function getOneFromCognito(string $cognito_id);
    public function store(array $data);
    public function update(array $data, string $id);
    public function delete(string $id);
}
