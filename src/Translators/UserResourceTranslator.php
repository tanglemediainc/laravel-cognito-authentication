<?php

namespace TangleMedia\Laravel\CognitoAuth\Translators;

use TangleMedia\Laravel\CognitoAuth\Http\Resources\UserResource;

class UserResourceTranslator
{

    public function translate($user) {
        $resource_class = config('cognito_auth.resources.user', UserResource::class);
        return new $resource_class($user);
    }

}
