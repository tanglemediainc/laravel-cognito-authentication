<?php

namespace TangleMedia\Laravel\CognitoAuth\Repositories;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TangleMedia\Laravel\CognitoAuth\Http\Controllers\AuthController;
use TangleMedia\Laravel\CognitoAuth\Http\Requests\UserUpdateAttributesRequest;
use TangleMedia\Laravel\CognitoAuth\Interfaces\Models\UserInterface;
use TangleMedia\Laravel\CognitoAuth\Interfaces\Repositories\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{

    protected $model;
    protected $request;
    protected $perPage;

    public function __construct(UserInterface $model, Request $request)
    {
        $this->model = $model;
        $this->request = $request;
        $this->perPage = config('users.pagination.per_page');
    }

    /**
     * Get results paginated (20 per page)
     * @return mixed
     */
    public function paginate()
    {
        return $this->model::filter($this->request)->paginate($this->perPage);
    }

    /**
     * Get single record
     * @param string $id
     * @return mixed
     */
    public function findOne(string $id)
    {
        $record = $this->model->where('id', $id)->first();
        if(!$record) throw new ModelNotFoundException("user_not_found");
        return $record;
    }

    /**
     * Get single record from username
     * @param string $cognito_id
     * @return mixed
     */
    public function findOneFromCognito(string $cognito_id)
    {
        $record = $this->model->where('cognito_id', $cognito_id)->first();
        if(!$record) throw new ModelNotFoundException("user_not_found");
        return $record;
    }

    /**
     * Create record
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model::create($data);
    }

    /**
     * Update record
     * @param array $data
     * @param string $id
     * @return mixed
     */
    public function update(array $data, string $id)
    {
        $record = $this->findOne($id);
        $record->update($data);
        $cognito_attrs = [];
        if(isset($data['name'])) {
            $cognito_attrs['name'] = $data['name'];
        }
        if(isset($data['email'])) {
            $cognito_attrs['email'] = $data['email'];
            $cognito_attrs['email_verified'] = 'true';
        }
        if(count($cognito_attrs)) {
            app(AuthController::class)->updateUser((new UserUpdateAttributesRequest(['username' => $record['cognito_id'], 'attributes' => $cognito_attrs])));
        }
        return $record->fresh();
    }

    /**
     * Delete record
     * @param string $id
     * @return mixed
     */
    public function delete(string $id)
    {
        return $this->findOne($id)->delete();
    }
}
