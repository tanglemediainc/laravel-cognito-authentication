<?php

namespace TangleMedia\Laravel\CognitoAuth\Services;

use TangleMedia\Laravel\CognitoAuth\Interfaces\Repositories\UserRepositoryInterface;
use TangleMedia\Laravel\CognitoAuth\Interfaces\Repository\UserServiceInterface;

class UserService implements UserServiceInterface
{
    protected $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get results paginated
     * @return mixed
     */
    public function getPaginated()
    {
        return $this->repository->paginate();
    }

    /**
     * Get one record
     * @param string $id
     * @return mixed
     */
    public function getOne(string $id)
    {
        return $this->repository->findOne($id);
    }

    /**
     * Get one record from username
     * @param string $cognito_id
     * @return mixed
     */
    public function getOneFromCognito(string $cognito_id)
    {
        return $this->repository->findOneFromCognito($cognito_id);
    }

    /**
     * Store new record
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->repository->create($data);
    }

    /**
     * Update an existing record
     * @param array $data
     * @param string $id
     * @return mixed
     */
    public function update(array $data, string $id)
    {
        return $this->repository->update($data, $id);
    }

    /**
     * Delete record
     * @param string $id
     * @return mixed
     */
    public function delete(string $id)
    {
        return $this->repository->delete($id);
    }
}
