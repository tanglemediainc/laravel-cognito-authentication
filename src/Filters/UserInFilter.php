<?php

namespace TangleMedia\Laravel\CognitoAuth\Filters;

class UserInFilter
{
    public function filter($builder, $value)
    {
        $ids = explode('|', $value);
        return $builder->whereIn('user_id', $ids);
    }
}
