<?php

namespace TangleMedia\Laravel\CognitoAuth\Filters;

class EmailFilter
{
    public function filter($builder, $value)
    {
        return $builder->where('email', '=', $value);
    }
}
