<?php

namespace TangleMedia\Laravel\CognitoAuth\Filters;

use App\Filters\AbstractFilter;

class UserFilter extends AbstractFilter
{
    protected $filters = [
        'email' => EmailFilter::class,
        'id' => IdentifierInFilter::class
    ];
}
