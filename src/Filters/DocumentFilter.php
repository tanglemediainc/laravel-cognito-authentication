<?php

namespace TangleMedia\Laravel\CognitoAuth\Filters;

use App\Filters\AbstractFilter;

class UserFilter extends AbstractFilter
{
    protected $filters = [
        'name' => NameFilter::class,
        'filename' => FilenameFilter::class
    ];
}
